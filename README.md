## Coffee Bar Simulation 

### Description

The purpose of this project is to simulate the activity of a coffeebar during 5 years and to analyze data from the coffeebar dataset.

Our project contains 6 python files. Two of them, "Useful_functions" and "Simulation", contain only functions called in the others files.

### How to run the project? 

Begin by running "Exploratory", then "Part2", "Part3" and finally, "Part4".

### Structure

The first file, "Exploratory", investigates the dataset and graphically visualizes some results. The plots and the text file containing the probabilities are stocked
in the directory "Results" :Probabilties.txt, total amount of sold drinks over the 5 years, total amount of sold food over the 5 years.

In the second file, "Part2", we create different types of customers : Customer, Unique, FromTripAdvisor, RegularReturning and Hipster. Each customer has different
attributes : an ID, a budget, a sales history and the prices. The Trip Advisor customer also has a tip as attribute. The method "buyDrinkAndFood" determines
what kind of drink and food the customer will buy based on the probabilities calculated in the file "Exploratory". The method stocks the result in the sales
history of the customer.

[Click here to see the structure of the class](https://drive.google.com/file/d/1IzxyfGHtiqoTxIjCovLmARM8hIKDg_fa/view?usp=sharing)

In the file "Part3", we simulate the activity of the coffeebar during 5 years and we make some plots that compare the simulation to the original dataset. 
The plots are stocked in the directory "Results": Comparison-total amount of sold drinks over the 5 years, Comparison-total amount of sold food over the 5 years, 
Average income per hour, Frequencies of returning customers. The simulation, called "Simulation_Data_Frame1" is also stocked in "Results".

In the file "Part4", we analyze the behaviour of the customers based on the original dataset and then we compare the original dataset to others simulations that 
modify the original situation. To realize these comparisons and analyses, we made some plots sotcked in "Results": Purchases of returning and one-time 
customers - Drinks, Purchases of returning and one-time customers - Food, Frequencies depending on the number of returning customers, Average income per hour
if number of returning is 50, Average income if prices go up by 20% from the beginning of 2015,Frequencies of returning customers depending on the budget, 
Average income pe hour if budget of hipsters drop to 40.

In the file "Useful_functions", two functions allow us to create some plots. Two others functions are used to generate and print the probabilities. Two functions 
permit to calculate the income. The two last functions, "frequency_returning_year" and "percentage_returning_oneTime_buy", are used to create some plots.

In the file "Simulation", there are two functions that permit to make the simulation of the coffeebar.

