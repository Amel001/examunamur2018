""" PART 1 """

import pandas as pd
import os
import collections
import copy
import matplotlib.pyplot as plt
from Code.Useful_functions import *

inputfile = os.path.abspath("../Data/Coffeebar_2013-2017.csv")
df = pd.read_csv(inputfile, sep=';')

"""PART 1"""
""" What food and drinks are sold by the coffee bar?"""

print (df['FOOD'].value_counts())
print (df['DRINKS'].value_counts())

#Answer: the coffee bar sells sandwiches, pies, cookies, muffins
#soda, coffee, water, milkshakes, tea, frappucino. """

""" How many unique customer did they have? """

print(len(df['CUSTOMER'].unique()))

# Answer : there are 247988 customers

""" Plots of the total amount of sold drinks over the 5 years? """

simple_plot(df, 'DRINKS','Total amount of sold drinks over the 5 years')

""" Plots of the total amount of sold food over the 5 years? """

simple_plot(df, 'FOOD','Total amount of sold food over the 5 years')

""" Average for each time and create a file "Probabilities """
probability_sentence_generetor()
