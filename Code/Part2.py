""" Part 2"""
import pandas as pd
import os
import random
from Code.Useful_functions import *


# Each customer has a customerID and a certain budget

class Customer(object):
    def __init__(self, ID, price, budget):
        self.ID = ID
        self.budget = budget
        self.command = {}
        self.price = price

    # tell the price of the food and drinks
    def give_price(self):
        return self.price

    # tell the amount they paid depending on the time
    def display_amountPayed (self, time):
        amountpayed = self.price[self.command[time]['DRINKS']]
        if (self.command[time]['FOOD'] != 'nothing'):
            amountpayed += self.price[self.command[time]['FOOD']]
        return amountpayed

    # tell the amount they paid in total
    def display_total_amount(self):
        TotalAmountPayed = 0
        for time in self.command :
            TotalAmountPayed += self.display_amountPayed(time)
        return TotalAmountPayed

    # given the correct probability at a specific time, the customers are able to buy drinks and food

    def buyDrinkAndFood (self, date, hour, dico):

        # Get all dictionnaries
        moment = str(date) + " " + str(hour)
        self.command[moment] = {}

        # Buy Drinks

        random_per = random.randint(1,100000000)
        dico_drinks = dico[hour]['DRINKS']
        percentage = 0

        for key, value in dico_drinks.items():
            if random_per <= percentage + round(value*1000000):
                self.command[moment]['DRINKS'] = key
                break
            else:
                percentage += round(value*1000000)

        # Buy food
        random_per = random.randint(1, 100000000)
        dico_food = dico[hour]['FOOD']
        percentage = 0

        for key, value in dico_food.items():
            if random_per <= percentage + round(value*1000000):
                self.command[moment]['FOOD'] = key
                break
            else:
                percentage += round(value*1000000)

        # Keep track of the budget
        self.command[moment]['BUDGET'] = self.display_amountPayed(moment)
        self.budget -= self.command[moment]['BUDGET']


        # Display what the customer has bought (food and drink) and the amount they payed

    # display what a customer ordered on a specific day and time
    def display_order(self, date, hour):
        moment = str(date) + " " + str(hour)
        sentence = "On the %s at %s, customer %s bought %s " %(date,hour,self.ID, self.command[moment]["DRINKS"])
        if self.command[moment]["FOOD"] != 'nothing':
            sentence += "and a %s." %(self.command[moment]["FOOD"])
        print(sentence + " Customer " + self.ID + " paid " + self.command[moment]["BUDGET"])

    # change price
    def change_price(self,new_price):
        self.price = new_price

class Unique(Customer):
    # characteristics : ID, budget = 100
    def __init__(self, ID, price, budget=100):
        super().__init__(ID, price, budget)

class FromTripadvisor(Unique):

    # characteristics : same as regular, tip
    def __init__(self, ID, price, budget = 100):
        super().__init__(ID, price, budget)
        self.tip = random.randint(1,10)

class RegularReturning(Customer):
    # characteristics : ID budget = 250
    def __init__(self, ID, price, budget = 250):
        super().__init__(ID, price, budget)

class Hipster(Customer):
    # characteristics : ID budget = 500
    def __init__(self, ID, price, budget=500):
        super().__init__(ID, price,budget)
