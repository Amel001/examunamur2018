""" PART 4"""
import os
import pandas as pd
import collections
import copy
from Code.Simulation import *
from Code.Useful_functions import *

inputfile = os.path.abspath("../Results/Simulations_Data_Frame1.csv")
simulation = pd.read_csv(inputfile, sep=';')

inputfile = os.path.abspath("../Data/Coffeebar_2013-2017.csv")
original = pd.read_csv(inputfile, sep=';')
original['FOOD'] = original['FOOD'].fillna('nothing')

"""Show some buying histories of returning customers for your simulations"""

First_historic = simulation[simulation['CUSTOMER']=='CID10000000']
Second_historic = simulation[simulation['CUSTOMER']=='CID10000030']
Third_historic = simulation[simulation['CUSTOMER']=='CID10000668']
Fourth_historic = simulation[simulation['CUSTOMER']=='CID10000700']

"""" How many returning customers? """

number_returning_customer = len(original['CUSTOMER'].value_counts().tolist())-collections.Counter(original['CUSTOMER'].value_counts().tolist())[1]


""" Do they have specific times when they show up more """
# List customers'  ID
ID = original['CUSTOMER'].value_counts().keys().tolist()

# New column 'HOUR'
copy_df = original.copy(deep=True)
copy_df['HOUR'] = copy_df['TIME'].str[11:]

# New data frame
returning_probabilities = copy_df[["HOUR","CUSTOMER"]].groupby(["HOUR","CUSTOMER"]).size().unstack()[ID[:1000]]
Results_probabilities = pd.DataFrame({'RETURNING': returning_probabilities.sum(axis=1)/18.25})
Results_probabilities ['UNIQUE'] = 100 - Results_probabilities['RETURNING']

# Maximal frequence
print("At %s, we have %s percent chance to see a returning customer"%  (Results_probabilities ['RETURNING'].idxmax(), round(Results_probabilities ['RETURNING'].values.max(),2)))

""" How does this impact their buying history? """

"""Since the returning customers show up more between 1 pm and 5 pm, and given the probabilities that a customer buys
 a specific food or drink at a specific time, the buying histories of the returning customers is more impacted by these probabilities
 at the times between 1 pm and 5 pm."""

"""Do you see correlations between what returning customers buy and one-timers?"""
copy = original.copy(deep=True)

Results_drinks = percentage_returning_oneTime_buy(copy, ID, "DRINKS")
Results_food = percentage_returning_oneTime_buy(copy, ID, "FOOD")
Results_food = Results_food.drop(['nothing'])

plot_comparison (Results_drinks['RETURNING'].tolist(),Results_drinks.index.values.tolist(),Results_drinks['ONE TIME'].tolist() ,
                 6,"maroon", "peachpuff", "RETURNING", "ONE TIME","Purchases of returning and one time customers - Drinks", "Drinks","Percentage")
plot_comparison (Results_food['RETURNING'].tolist(),Results_food.index.values.tolist(),Results_food['ONE TIME'].tolist() ,
                 4,"maroon", "peachpuff", "RETURNING", "ONE TIME","Purchases of returning and one time customers - Food", "Food","Percentage")

""" What would happen if we lower the returning customers to 50 and simulate the same period? """

# Data frame creation
dico = dico_probability_generetor()
original_price = {'sandwich' :5, 'cookie' : 2, 'muffin' : 3, 'pie':3, 'soda':3, 'frappucino' :4,'water':2, 'milkshake':5, 'coffee':3, 'tea':3}
DF,List_returning = Simulations(returning_creation(50,original_price), dico,original_price, original['TIME'].tolist())

# GRAPH RETURNING CUSTOMERS

plot_comparison (frequency_returning_year(DF),simulation['TIME'].str[0:4].unique(), frequency_returning_year(simulation),
                 5,"maroon", "peachpuff", "Simulation with 50 returning customers", "Simulation with 1000 returning","Frequencies depending on the number of returning customers", "Year","Frequency")

plot_comparison(total_income_per_hour(income_per_hour(original,'DRINKS', original_price), income_per_hour(original,'FOOD', original_price)),original['TIME'].str[11:13].unique(),
                total_income_per_hour(income_per_hour(DF,'DRINKS', original_price), income_per_hour(DF,'FOOD', original_price)),
                10,"maroon", "peachpuff", "Original", "Simulation","Average income per hour if number of returning is 50", "Hour","Average income")


""" The prices change from the beginning of 2015 and go up by 20% """

price = {'sandwich' :6, 'cookie' : 2.4, 'muffin' : 3.6, 'pie':3.6, 'soda':3.6, 'frappucino' :4.8,'water':2.4, 'milkshake':6, 'coffee':3.6, 'tea':3.6}
DF_2013_2015, List_returning = Simulations(returning_creation(1000,original_price),dico, original_price,original['TIME'].tolist()[:124830])
for customer in List_returning:
    customer.change_price(price)
DF_2015_2017, List_returning = Simulations(List_returning, dico,price,original['TIME'].tolist()[124830:])
DF_2013_2017 = pd.concat([DF_2013_2015, DF_2015_2017], ignore_index= True)

total_income = total_income_per_hour(total_income_per_hour(income_per_hour(DF_2013_2015,'DRINKS', original_price), income_per_hour(DF_2013_2015,'FOOD', original_price)),
                                     total_income_per_hour(income_per_hour(DF_2015_2017, 'DRINKS', price),income_per_hour(DF_2015_2017, 'FOOD', price)))


plot_comparison (total_income_per_hour(income_per_hour(original,'DRINKS', original_price),income_per_hour(original,'FOOD', original_price)),
                 original['TIME'].str[11:13].unique(),total_income,
                 10,"maroon", "peachpuff", "Original", "Simulation","Average income per hour if prices go up by 20% from the beginning of 2015", "Hour","Average income")


""" The budget of hipsters drops to 40"""
DF3, List_returning = Simulations(returning_creation(1000,original_price, 40), dico,original_price,original['TIME'].tolist())
plot_comparison (frequency_returning_year(original),original['TIME'].str[0:4].unique(), frequency_returning_year(DF3),
                 5,"maroon", "peachpuff", "Original : Hispters have 500" ,"Simulation : Hispters have 40" ,"Frequencies of returning customers depending on their budget", "Year","Frequency")

plot_comparison(total_income_per_hour(income_per_hour(original,'DRINKS', original_price), income_per_hour(original,'FOOD', original_price)),original['TIME'].str[11:13].unique(),
                total_income_per_hour(income_per_hour(DF3,'DRINKS', original_price), income_per_hour(DF3,'FOOD', original_price)),
                10,"maroon", "peachpuff", "Original", "Simulation","Average income per hour if budget of hipster drops to 40", "Hour","Average income")



""" If the shop is open from 7:30 to 18:28 """

# date
list_date1 = []
for date in original['TIME'].str[:10].unique():
    for hour in ['07:30:00', '07:35:00', '07:40:00', '07:45:00', '07:50:00', '07:55:00', '18:00:00', '18:04:00', '18:08:00', '18:12:00', '18:16:00', '18:20:00', '18:24:00', '18:28:00']:
        list_date1.append(date +' ' + hour)

list_date1 += original['TIME'].tolist()
list_date1.sort()

# dico
dico = dico_probability_generetor()
for hour in ['07:30:00', '07:35:00', '07:40:00', '07:45:00', '07:50:00', '07:55:00']:
    dico[hour] = dico ['08:' + hour[3:5]  + ':00']
for hour in ['18:00:00', '18:04:00', '18:08:00', '18:12:00', '18:16:00', '18:20:00', '18:24:00', '18:28:00']:
    dico[hour] = dico['17:' + hour[3:5] + ':00']

DF4, List_returning = Simulations(returning_creation(1000, original_price), dico, original_price, list_date1)

print("The average income per year is %.2f € if the shop is open from 7:30 to 18:28 and %.2f € if the shop is open from 8:00 to 18:00."
% (average_income_per_year(DF4, original_price),average_income_per_year(original, original_price)))