""" Functions useful for the simulation"""

from Code.Part2 import *
import pandas as pd
import os

""" 1 : Create returning customers """

def returning_creation (number_returning,price,budget_hipster=500, budget_returning=250):
    list_returning_customers = []
    for number in range(10000000,10000000+number_returning):
        ID = "CID" + str(number)
        if number <= (10000000+((number_returning*2)/3)):
            list_returning_customers.append(RegularReturning(ID,price, budget_returning))

        else:
            list_returning_customers.append(Hipster(ID,price,budget_hipster))

    return list_returning_customers

""" 2 : Simulation """

def Simulations(List_returning_customers, dico,price, list_date):

    # Generate variables
    list_data = []
    last_ID = 10001001
    List_returning_customers = copy.deepcopy(List_returning_customers)

    # Create a customer for each hour
    for date in list_date:
        percentage = random.randint(1, 100)

        # Create a returning customer (20%), trip advisor customer (8%) or unique customer (72%)
        if percentage <= 20 :
            if len(List_returning_customers) == 0 :
                customer = None
            else :
                customer = random.choice(List_returning_customers)
        elif 20 < percentage <= 28:
            customer = FromTripadvisor("CID" + str(last_ID), price)
            last_ID += 1
        else:
            customer = Unique("CID" + str(last_ID), price)
            last_ID += 1

        # Given the customer and the time, give what the customer buys
        if customer != None :
            customer.buyDrinkAndFood(date[:10], date[11:], dico)

        # Create a new row for the data frame
        if customer != None:
            list_data.append({"TIME": date,
                          "CUSTOMER": customer.ID,
                          "DRINKS": customer.command[date]["DRINKS"],
                          "FOOD": customer.command[date]["FOOD"]})
        else:
            list_data.append({"TIME": date,
                          "CUSTOMER": None,
                          "DRINKS": None,
                          "FOOD": None})

        # Check, if it's a returning, that the budget is high enough
        if len(List_returning_customers) != 0 :
            if (int(customer.ID[3:]) < 10001001) and (customer.budget < 10):
                List_returning_customers.remove(customer)

    DF = pd.DataFrame(list_data, columns=["TIME", "CUSTOMER", "DRINKS", "FOOD"])
    return DF, List_returning_customers
