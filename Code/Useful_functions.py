""" In this script : useful function"""

import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import copy

""" PLOTS """

def simple_plot(Data_Frame, column,title):
    values = Data_Frame[column].value_counts().keys().tolist()
    counts = Data_Frame[column].value_counts().tolist()

    fig, ax = plt.subplots()
    ax.bar(values, counts)
    ax.set_title(title, style = 'italic', weight = 'semibold')

    plt.savefig("../Results/"+ title +".png")
    plt.show()

def plot_comparison(first_counts, first_value, second_counts, number_bars, color_1, color_2, label_1, label_2,title, x_label, y_label):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    width = 0.35
    ind = np.arange(number_bars)

    # bars
    rects1 = ax.bar(ind, first_counts, width, color=color_1)
    rects2 = ax.bar(ind + width, second_counts, width, color=color_2)
    ax.set_xlim(-width, len(ind) + width)

    xTickMarks = first_value
    ax.set_xticks(ind + 0.17)
    xtickNames = ax.set_xticklabels(xTickMarks)
    plt.setp(xtickNames, rotation=0, fontsize=10)

    ax.legend((rects1[0], rects2[0]), (label_1, label_2))
    ax.set_title(title, style='italic', weight='semibold')
    ax.set_xlabel(x_label, style='italic', weight='ultralight')
    ax.set_ylabel(y_label, style='italic', weight='ultralight')

    plt.savefig("../Results/" + title + ".png")
    plt.show()


""" PROBABILITIES """

def dico_probability_generetor ():

    inputfile = os.path.abspath("../Data/Coffeebar_2013-2017.csv")
    df = pd.read_csv(inputfile, sep=';')

    df_copy = df.copy(deep=True)

    # Replace NaN in the column FOOD by "nothing"
    df_copy['FOOD'] = df_copy['FOOD'].fillna("nothing")

    # Create a column with only the hour
    df_copy['HOUR'] = df_copy['TIME'].str[-8:]

    # Create a Data Frame with the hour, the food and Drinks (separately) and the occurence of each element for each hour
    df_hour_drinks = df_copy[['TIME', 'HOUR', 'DRINKS']].groupby(['HOUR', 'DRINKS'], as_index=False).count()
    df_hour_food = df_copy[['TIME', 'HOUR', 'FOOD']].groupby(['HOUR', 'FOOD'], as_index=False).count()

    # Create a list with all the hour
    list_hour = df_hour_drinks['HOUR'].value_counts().keys().tolist()
    list_hour.sort()

    # Get a list with all the food
    list_basic_food = df_copy['FOOD'].value_counts().keys().tolist()
    list_basic_food.sort()

    # Create an empty dictionnary
    dico = {}

    for hour in list_hour:

        # Create keys inside the dictionnary
        dico[hour] = {'DRINKS':{},'FOOD':{}}

        # Get data relative to drinks and to time. Calculate the total
        df_per_hour = df_hour_drinks[df_hour_drinks['HOUR'] == hour][['DRINKS', 'TIME']]
        list_drinks = df_per_hour['DRINKS'].tolist()
        list_nombre = df_per_hour['TIME'].tolist()
        total_drink = sum(list_nombre)

        # Do the same with food
        df_per_hour = df_hour_food[df_hour_food['HOUR'] == hour][['FOOD', 'TIME']]
        list_nombre_food = df_per_hour['TIME'].tolist()
        list_food = df_per_hour['FOOD'].tolist()
        total_food = sum(list_nombre_food)

        i = 0

        for index1 in range(6):
            # DRINKS
            pourcentage_drink = (((list_nombre[index1]) / total_drink) * 100)
            drink = list_drinks[index1]
            dico[hour]['DRINKS'][drink] = pourcentage_drink

            # FOOD
            pourcentage_food = 0
            if index1 < 5:
                if list_basic_food[index1] in list_food:
                    pourcentage_food = (((list_nombre_food[i]) / total_food) * 100)
                    i+=1
                dico[hour]['FOOD'][list_basic_food[index1]] = pourcentage_food

    return dico

def probability_sentence_generetor():
    dico_elements = dico_probability_generetor()

    file = open('..\Results\Probabilities.txt','w')

    for hours in dico_elements:
        sentence3 = "On average the probability of a customer at %s buying tea, coffee, soda, milkshake, water and frappucino is " % (str(hours))
        dico_elements_drinks = dico_elements[hours]['DRINKS']

        sentence3 += str(round(dico_elements_drinks["tea"],1)) + " %, " + str(round(dico_elements_drinks["coffee"],1)) + " %, " + str(round(dico_elements_drinks["soda"],1)) + " %, " + str(round(dico_elements_drinks["milkshake"],1)) + "%, "
        sentence3 += str(round(dico_elements_drinks["water"],1)) + " %, " + str(round(dico_elements_drinks["frappucino"],1)) + " % and for food "

        for foods in dico_elements[hours]['FOOD']:
            if dico_elements[hours]['FOOD'][foods] != 0:
                sentence3 += str(round(dico_elements[hours]['FOOD'][foods],1)) + " % " + foods + ", "

        sentence3 = sentence3 [:-2] + "."

        file.write(sentence3+'\n')

        #print (sentence3)

    file.close()


""" INCOME """

# Calculate total income for a product """
def income_per_hour(Data_Frame,column, prix):
    Data_Frame['HOUR'] = Data_Frame['TIME'].str[11:13]
    DF_element = Data_Frame[['TIME', 'HOUR', column]].groupby(['HOUR', column], as_index=False).count()

    list_hour = []
    for hour in DF_element['HOUR'].unique().tolist():
        df_hour = DF_element[DF_element['HOUR'] == hour][[column, 'TIME']]
        Total_amount = 0
        for element in df_hour[column].tolist():
            if element != 'nothing':
                Total_amount += df_hour[df_hour[column] == element]['TIME'].iloc[0] * prix[element]

        list_hour.append(Total_amount / 1825)
    return list_hour

# Total income per Hour for all products"""
def total_income_per_hour(income_drinks, income_food):

    total_income = []

    for index in range(len(income_drinks)):
        total_income.append(income_drinks[index] + income_food[index])
    return total_income

# Average income per year
def average_income_per_year(Data_Frame, price):
    DF_drinks = Data_Frame[['DRINKS', 'TIME']].groupby(['DRINKS'], as_index=False).count()
    DF_food = Data_Frame[['FOOD', 'TIME']].groupby(['FOOD'], as_index=False).count()

    for i, row in DF_drinks.iterrows():
        DF_drinks.at[i, 'TIME'] = DF_drinks['TIME'][i] * price[row['DRINKS']]

    for i2, row2 in DF_food.iterrows():
        if row2['FOOD'] != 'nothing':
            DF_food.at[i2, 'TIME'] = DF_food['TIME'][i2] * price[row2['FOOD']]

    return (DF_drinks['TIME'].sum() + DF_food['TIME'].sum())/5

""" GENERATE VALUE FOR RETURNING CUSTOMERS """

def frequency_returning_year(Data_Frame):
    Data_Frame_copy = Data_Frame.copy(deep = True)
    Data_Frame_copy['YEAR'] = Data_Frame_copy['TIME'].str[0:4]
    list_frequencies = []

    for year in Data_Frame_copy['YEAR'].unique().tolist():
        list_counts = Data_Frame_copy[Data_Frame_copy['YEAR'] == year]['CUSTOMER'].value_counts().tolist()
        index = 0
        frequency_per_year = 0

        while list_counts[index] > 1:
            frequency_per_year += list_counts[index]
            index += 1
        list_frequencies.append(frequency_per_year)

    return list_frequencies


""" GET PERCENTAGE OF WHAT RETURNING AND ONE TIME CUSTOMERS BUY """

def percentage_returning_oneTime_buy(DataFrame, Customers_ID, products):
    all_customer = DataFrame[[products, "CUSTOMER"]].groupby([products, "CUSTOMER"]).size().unstack()
    one_time = all_customer[Customers_ID[1000:]]
    returning = all_customer[Customers_ID[:1000]]
    Results = pd.DataFrame({'RETURNING': returning.sum(axis=1), 'ONE TIME': one_time.sum(axis=1)})
    Results['RETURNING'] = Results['RETURNING'] / (Results['RETURNING'].sum() * 0.01)
    Results['ONE TIME'] = Results['ONE TIME'] / (Results['ONE TIME'].sum() * 0.01)

    return Results