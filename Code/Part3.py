""" PART 3 : SIMULATIONS """
from Code.Part2 import *
from Code.Useful_functions import *
import copy
import pandas as pd
import os
import collections
from Code.Simulation import *

""" First : Simulation """

inputfile = os.path.abspath("../Data/Coffeebar_2013-2017.csv")
First_df = pd.read_csv(inputfile, sep=';')

# Generate the dico with probabilities and the list with prices
dico = dico_probability_generetor()
price = {'sandwich' :5, 'cookie' : 2, 'muffin' : 3, 'pie':3, 'soda':3, 'frappucino' :4,'water':2, 'milkshake':5, 'coffee':3, 'tea':3}

# Try a simulation and save in a file
Second_df, List_returning = Simulations(returning_creation (1000,price), dico,price,First_df['TIME'].tolist())
Second_df.to_csv("../Results/Simulations_Data_Frame1.csv", sep=';',index=False)


""" Second : plots """

# GRAPH DRINKS

plot_comparison(First_df['DRINKS'].value_counts().tolist(), First_df['DRINKS'].value_counts().keys().tolist(), Second_df['DRINKS'].value_counts().tolist(),
                6, "maroon", "peachpuff", "Original", "Simulation"," Comparison - Total amount of sold drinks over the 5 years","Kind of drinks","Amount sold")

# GRAPH FOOD

plot_comparison(First_df['FOOD'].value_counts().tolist(), First_df['FOOD'].value_counts().keys().tolist(),(Second_df['FOOD'].value_counts().tolist())[1:],
                4, "maroon", "peachpuff", "Original", "Simulation","Comparison - Total amount of sold food over the 5 years","Kind of food","Amount sold")

# GRAPH INCOME PER HOUR

First_df['FOOD'] = First_df['FOOD'].fillna('nothing')
price= {'sandwich' :5, 'cookie' : 2, 'muffin' : 3, 'pie':3, 'soda':3, 'frappucino' :4,'water':2, 'milkshake':5, 'coffee':3, 'tea':3}

First_df_income = total_income_per_hour(income_per_hour(First_df,'DRINKS', price), income_per_hour(First_df,'FOOD', price))
Second_df_income = total_income_per_hour(income_per_hour(Second_df,'DRINKS',price),income_per_hour(Second_df,'FOOD', price))

plot_comparison (First_df_income,First_df['TIME'].str[11:13].unique(), Second_df_income, 10,"maroon", "peachpuff", "Original", "Simulation","Average income per hour", "Hour","Average income")

# GRAPH RETURNING CUSTOMERS

plot_comparison (frequency_returning_year(First_df),First_df['TIME'].str[0:4].unique(), frequency_returning_year(Second_df),
                 5,"maroon", "peachpuff", "Original", "Simulation","Frequencies of returning customers", "Year","Frequency")
